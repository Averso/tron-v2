#include "Board.h"


Board::Board()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			field[i][j] = 0;
		}
	}
}
int Board::get(int x, int y)// x - wsp�rz�dna w poziomie, y - wsp�rz�dna w pionie 
{
	if (x >= 0 && x < M && y >= 0 && y < N)
	{
		return field[y][x];
	}
	return -1;
}
bool Board::set(int x, int y, int player)
{
	if (player > 0 && player < 5 && field[y][x] == 0 && x< M && y<N && x > -1)
	{
		field[y][x] = player;
		return true;
	}
	else 
		return false;
}
void Board::clear()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			field[i][j] = 0;
		}
	}
}

//int** Board::getField()
//{
//	return *field[];
//}


unsigned int Board::getWidth()
{
	return width;
}

unsigned int Board::getHeight()
{
	return height;
}

void Board::copyField(int out[N][M])
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			out[i][j] = field[i][j];
}