#ifndef Screen_h
#define Screens_h
#include<SDL.h>
#include<SDL_ttf.h>
#include"Texture.h"
#include"Defines.h"
//tu bd struktury z roznymi ekranami tj textury i napisy dla ekranow
//zeby to sie nie walalo w maine


///TRZEBA TO LADNIE W SETY GETY ITP
struct ScreenMain
{
public:
	Texture background;
	TTF_Font* font = NULL;

};
typedef struct ScreenMain ScreenMain;

struct ScreenGameOver
{
public:
	Texture background;
	SDL_Rect rect;
	Texture gameOverText;
	Texture restartText;
	Texture quitText;

	ScreenGameOver()
	{
		rect = { 0, 0, BASE_SCREEN_WIDTH, BASE_SCREEN_HEIGHT };

	}

	
	//Texture ... tu texty
	void free()
	{
		background.free();
		gameOverText.free();
		restartText.free();

	}

	//Tu trzeba jaki� schemat �rodkowania
	void render(SDL_Renderer* winRenderer)
	{
		background.render(&rect, winRenderer, 0);

		//ustawienie pozycji tekstu
		int textX = (BASE_SCREEN_WIDTH - gameOverText.getWidth()) / 2;
		int textY = ((BASE_SCREEN_HEIGHT - gameOverText.getHeight()) / 2);
		SDL_Rect rectText = { textX, textY-30, gameOverText.getWidth(), gameOverText.getHeight() };
		gameOverText.render(&rectText, winRenderer, 0);

		textX = (BASE_SCREEN_WIDTH - restartText.getWidth()) / 2;
		textY = (BASE_SCREEN_HEIGHT - restartText.getHeight()) / 2;
		rectText = { textX, textY, restartText.getWidth(), restartText.getHeight() };
		restartText.render(&rectText, winRenderer, 0);

		textX = (BASE_SCREEN_WIDTH - quitText.getWidth()) / 2;
		textY = (BASE_SCREEN_HEIGHT - quitText.getHeight()) / 2;
		rectText = { textX, textY + 25, quitText.getWidth(), quitText.getHeight() };
		quitText.render(&rectText, winRenderer, 0);

	}

	bool loadTexts(const char * path, SDL_Renderer* winRenderer)
	{
		TTF_Font* font = TTF_OpenFont(path, 50);


		if (font == NULL)
		{
			printf("Problem z wczytaniem czcionki %s:  %s.\n", path, TTF_GetError());
			return false;
		}


		

		if (!gameOverText.loadFromRenderedText(font, winRenderer, "GAME OVER", { 255, 255, 255, 255 }))
		{
			printf("Problem z wczytaniem tekstu\n");
			return false;
		}

		font = TTF_OpenFont(path, 40);
		if (!restartText.loadFromRenderedText(font, winRenderer, "Wcisnij R by zrestartowac", { 255, 255, 255, 255 }))
		{
			printf("Problem z wczytaniem tekstu\n");
			return false;
		}

		if (!quitText.loadFromRenderedText(font, winRenderer, "Wcisnij ESC by wyjsc", { 255, 255, 255, 255 }))
		{
			printf("Problem z wczytaniem tekstu\n");
			return false;
		}
	}

};
typedef struct ScreenGameOver ScreenGameOver;


//tu sie zainputuje tez wyniki czy cos, zeby wyswietlal
struct ScreenGame
{
	
public:
	int boardX = BOARD_X;
	int boardY = BOARD_Y;
	int boardW = BOARD_WIDTH;
	int boardH = BOARD_HEIGHT;

	int sideBarX = BOARD_X + BOARD_WIDTH;
	int sideBarY = BOARD_Y;
	int sideBarW = BASE_SCREEN_WIDTH - BOARD_WIDTH;
	int sideBarH = BOARD_HEIGHT;
	Texture background;
	Texture sideBar;
	SDL_Rect rectBg;
	SDL_Rect rectSb;
	TTF_Font* font = NULL;
	ScreenGame()
	{
		rectBg = { boardX,boardY,boardW,boardH};
		rectSb = { sideBarX, sideBarY, sideBarW, sideBarH };
	}

	//Texture ... tu texty
	void free()
	{
		background.free();

	}
	void render(SDL_Renderer* winRenderer)
	{
		background.render(&rectBg, winRenderer, 0);
		sideBar.render(&rectSb, winRenderer,0);
	}
};
typedef struct ScreenGame ScreenGame;

#endif