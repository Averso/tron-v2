#include "communication.h"


communication::communication(char* ip, int port) //
{
	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != NO_ERROR)
		printf("Initialization error.\n");
	mainSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mainSocket == INVALID_SOCKET)
	{
		printf("Error creating socket: %ld\n", WSAGetLastError());
		WSACleanup();
	}

	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);
}

communication::communication(SOCKET mainSocketIn)
{
	this->mainSocket = mainSocketIn;
}

void communication::conect()
{
	if (connect(mainSocket, (SOCKADDR *)& service, sizeof(service)) == SOCKET_ERROR)
	{
		printf("Failed to connect.\n");
		WSACleanup();
	}
}

void communication::sendDirection(direction toSend)
{
	send(mainSocket, (char *)&toSend, sizeof(direction), 0);
}

void communication::sendRecevData(recevData toSend)
{
	send(mainSocket, (char *)&toSend, sizeof(recevData), 0);
}

direction communication::receiveDirection()
{
	char recbuf[sizeof(direction)];
	direction tmp;
	recv(mainSocket, recbuf, sizeof(direction), 0);
	memcpy(&tmp, recbuf, sizeof(direction));
	return tmp;
}

recevData communication::receiveRecevData()
{
	char recbuf[sizeof(recevData)];
	recevData tmp;
	recv(mainSocket, recbuf, sizeof(recevData), 0);
	memcpy(&tmp, recbuf, sizeof(recevData));
	return tmp;
}


server::server(char* ip, int port, int numberOfPleyersIn, CRITICAL_SECTION CriticalSection)
{
	data = new dataToThread[numberOfPleyersIn];
	thread = new HANDLE[numberOfPleyersIn];

	this->numberOfPlayers = numberOfPleyersIn;
	this->CriticalSection = CriticalSection;


	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != NO_ERROR)
		printf("Initialization error.\n");
	mainSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mainSocket == INVALID_SOCKET)
	{
		printf("Error creating socket: %ld\n", WSAGetLastError());
		WSACleanup();
	}

	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);
}

void server::acceptPleyers()
{
	DWORD id;

	if (bind(mainSocket, (SOCKADDR *)& service, sizeof(service)) == SOCKET_ERROR)
	{
		printf("bind() failed.\n");
		closesocket(mainSocket);
	}

	if (listen(mainSocket, 1) == SOCKET_ERROR)
		printf("Error listening on socket.\n");

	SOCKET tmpSocket = SOCKET_ERROR;
	printf("Waiting for a client to connect...\n");


	for (int i = 0; i < numberOfPlayers; i++)
	{
		
		data[i].mySocket = accept(mainSocket, NULL, NULL);
		data[i].next = false;
		data[i].send = false;
		data[i].CriticalSection = CriticalSection;
		
		thread[i] = CreateThread(
			NULL, // atrybuty bezpiecze�stwa
			0, // inicjalna wielko�� stosu
			run, // funkcja w�tku
			(void *)&data[i],// dane dla funkcji w�tku
			0, // flagi utworzenia
			&id);
		if (thread[i] != INVALID_HANDLE_VALUE)
		{
			SetThreadPriority(thread[i], THREAD_PRIORITY_NORMAL);
		}
		
	}
	
}

dataToThread* server::getData()
{
	return data;
}

DWORD WINAPI run(void *argumenty)
{
	int i = 0;
	struct dataToThread *data = (struct dataToThread *)argumenty;
	communication conect(data->mySocket);
	direction tmp;
	recevData toSend;

	EnterCriticalSection(&data->CriticalSection);
	conect.sendRecevData(data->toSend);
	conect.sendDirection(data->direction);
	LeaveCriticalSection(&data->CriticalSection);

	while (1)
	{
		tmp = conect.receiveDirection();

		EnterCriticalSection(&data->CriticalSection);
		data->direction = tmp;
		data->next = true;
		LeaveCriticalSection(&data->CriticalSection);


		
		while (1) //oczekiwanie na gotowo�c planszy do wsy�ania
		{
			EnterCriticalSection(&data->CriticalSection);
			if (data->send == true)
				break;
			LeaveCriticalSection(&data->CriticalSection);
		}


		EnterCriticalSection(&data->CriticalSection);
		conect.sendRecevData(data->toSend);
		data->send = false;
		LeaveCriticalSection(&data->CriticalSection);
	}
}

