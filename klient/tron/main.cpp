/*
1. zalaczaenie SDL'a: https://www.youtube.com/watch?v=I-yOf4Xg_R8
-on wrzuca dll-ke do debuga, a powino sie tam gdzie main
-sdl image załączamy analogicznie, wrzucamy folder, ustawiamy miejsce includow, libow, tak gdzie sdl2.lib itp wpisywal
wpisujemy sdl_im...lib *(to z folderu x86), i po buildzie kopiujemy wszystkie dll to folderu z mainem
2. http://lazyfoo.net/tutorials/SDL/

*/
#define _CRT_SECURE_NO_WARNINGS
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <string>


#include "Defines.h"
#include "Texture.h"
#include "Player.h"
#include "Board.h"
#include "Game.h"
#include "Timer.h"
#include "Screens.h"


enum gamemode  //lista ekranów
{
	menu,		//menu główne
	game,	//gra
	gameover,	//ekran przegranej
	win			//ekran wygranej
} gamemode;


bool init();		//inicjalizacja - sdl,okno itp
bool loadFiles();	//wczytanie plikow - tekstury itp
void close();		//zwolnienie powierzchni, pamieci itp.


SDL_Window* window = NULL;	//wskaznik na okno
SDL_Renderer* winRenderer = NULL;	//tzw renderer - on rysuje w oknie
Player *player1 = new Player(1, 0, 0);	//gracz 1

//ekrany w strukturach - patzr screens_h
ScreenGame screenGame;
ScreenGameOver screenGameOver;


double coun = 0.0;
int direction = 1; //prawo - 1, góra - 2, lewo - 3, dół - 4 




bool fun(Player & player, Board & board){
	//coun += 1;
	//double ww = coun - (int)coun;
	//int tmp = ww * 10;
//	if (tmp == 0){
		player.newPosition();
		if (!board.set(player.getX(), player.getY(), player.getId()))
			return false;
//	}

		
		return true;
}


int main(int argc, char* args[])
{
	
	//tutaj zapisuje wszelkie printfy itp - do sprawdzania bledow
	FILE * crashlog = freopen("crashlog.txt", "wb", stdout);

	Board board;	//obiekt planszy
	Game currentGame;
	
	currentGame.players[0] = *player1;
	currentGame.board = &board;
	gamemode = game;

	if (!init())
	{
		printf("Problem w inicjalizacji\n");
	}
	else
	{
		if (!loadFiles())
		{
			printf("Problem z wczytaniem plikow\n");
		}
		else
		{
			
			bool quit = false;
			SDL_Event e;	//do obslugi eventow
			

			//PETLA GLOWNA
			while (!quit)
			{
				
					if (gamemode == game)
					{
						while (SDL_PollEvent(&e) != 0)
						{

							if (e.type == SDL_QUIT)
							{
								quit = true;
							}
							else if (e.type == SDL_KEYDOWN) //obsluga klawiatury
							{
								switch (e.key.keysym.sym)
								{
								case SDLK_UP:
									if (player1->getDirectionY() != 1)
									{
										player1->setDirectionX(0);
										player1->setDirectionY(-1);
									}
									break;

								case SDLK_DOWN:
									if (player1->getDirectionY() != -1)
									{
										player1->setDirectionX(0);
										player1->setDirectionY(1);
									}
									break;

								case SDLK_LEFT:
									if (player1->getDirectionX() != 1)
									{
										player1->setDirectionX(-1);
										player1->setDirectionY(0);
									}
									break;

								case SDLK_RIGHT:
									if (player1->getDirectionX() != -1)
									{
										player1->setDirectionX(1);
										player1->setDirectionY(0);
									}
									break;

								case SDLK_ESCAPE:	//zakonczenie
									quit = true;
									break;
								default:
									break;

								}
							}
						}

						//Czyszczenie ekranu
						
						SDL_RenderClear(winRenderer);

						//renderowanie gracze i sladow
						screenGame.render(winRenderer);

						//ruszanie gracza
						if (!fun(*player1, board))
						{
							gamemode=gameover;
						}


						currentGame.renderTrails(player1, winRenderer);
						player1->renderVehicle(winRenderer, 0);

						//Update screen
						SDL_RenderPresent(winRenderer);
					}
					else if (gamemode = gameover)
					{
						while (SDL_PollEvent(&e) != 0)
						{

							if (e.type == SDL_QUIT)
							{
								quit = true;
							}
							else if (e.type == SDL_KEYDOWN) //obsluga klawiatury
							{
								switch (e.key.keysym.sym)
								{
								case SDLK_r:
									board.clear();
									player1->reset();
									gamemode = game;
									break;
								case SDLK_ESCAPE:	//zakonczenie
									quit = true;
									break;
								default:
									break;

								}
							}
						}

						//Czyszczenie ekranu						
						SDL_RenderClear(winRenderer);

						screenGameOver.render(winRenderer);
						//Update screen
						SDL_RenderPresent(winRenderer);
					}
					

				}
			}
		}

		//Free resources and close SDL
		close();
		fclose(crashlog);	//zampkij plik
		return 0;
}


bool init()
{
	//właczenie sdla
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Blad inicjalizacji SDL: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		//wlaczenie filtrowania liniowego - do tekstu
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Filtrowanie liniowe NIE wlaczane.");
		}

		//Tworzenie okna
		window = SDL_CreateWindow("TRON", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Problem ze stworzeniem okna SDL Error: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			//renderer dla okna z v sync
			winRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (winRenderer == NULL)
			{
				printf("Problem z stworzeniem renderera: %s\n", SDL_GetError());
				return  false;
			}
			else
			{
				//Kolor tla
				SDL_SetRenderDrawColor(winRenderer, 0xFF, 0x11, 0x01, 0x11);

				//WAZNE - skala - jezeli zmienimy rozmiar okna to renderer bedzie odpowiednio
				//skalowal wszystko co rysuje - czyli mozna dowolnie ustawiać rozmiar okna
				//848 i 480 bo to pierwotny rozmiar, pod niego sa sprity
				//no i generalnie potem trzeba jakos skorelowac wielkosc tablicy do wymiarow, ale to potem
				SDL_RenderSetScale(winRenderer, (double)SCREEN_WIDTH / (double)BASE_SCREEN_WIDTH, (double)SCREEN_HEIGHT / (double)BASE_SCREEN_HEIGHT);

				//mozliwosc wczytywania plikow png
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("Problem z inicjalizacja SDL_Image: %s\n", IMG_GetError());
					return false;
				}
				//init czcionek
				if (TTF_Init() == -1)
				{
					printf("Problem z inicjalizacja SDL_ttf: %s\n", TTF_GetError());
					return false;
				}
			}
		}
	}

	return true;
}


bool loadFiles()
{
	//tekstura pojazdu gracza
	if (!player1->loadVehTexture("spr/player1.png", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"player1.png\".\n");
		return false;
	}
	//tekstura 
	if (!player1->loadTrailTexture("spr/player1trail.png", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"player1trail.png\".\n");
		return false;
	}

	if (!screenGame.background.loadFromFile("spr/backgroundMaingif.gif", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"spr/backgroundGame.png\".\n");
		return false;
	}

	if (!screenGame.sideBar.loadFromFile("spr/sidebar.png", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"spr/sidebar.png\".\n");
		return false;
	}

	if (!screenGameOver.background.loadFromFile("spr/backgroundMain.png", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"spr/backgroundMain.png\".\n");
		return false;
	}

	if (!screenGameOver.loadTexts("fonts/tron2.ttf",winRenderer))
	{
		return false;
	}

	

	return true;
}


void close()
{
	//Free loaded images
	player1->freeTextures();

	screenGame.free();
	screenGameOver.free();
	//Destruktor okna i renderera
	SDL_DestroyRenderer(winRenderer);
	SDL_DestroyWindow(window);
	window = NULL;
	winRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

