#ifndef Game_h
#define Game_h
#include<SDL.h>
#include<SDL_ttf.h>

#include "Board.h"
#include "Player.h"
#include "Defines.h"

class Game {
public:
	Board * board;
	Player players[PLAYER_NUM];


	void renderTrails(Player * p, SDL_Renderer* winRenderer);	//renderowanie sladow

};



#endif