#ifndef Board_h
#define Board_h
#include"Player.h"
#include"Defines.h"

class Board {
private: 
	
	unsigned const int width = M;
	unsigned const int height = N;
public:
	
	Board();
	int get(int x, int y);
	bool set(int x, int y, int player);
	void clear();

	//TABLICA JEST TU
	int field[N][M]; //moglaby sie nazywac w sumie board, ale co wazniejsze
	int ** getField();// to chyba lepiej ja alokowac w konstruktorze
						//bo inaczej chyab sie nie da jej getowa� wyskakuje, �e typy nie pasuja
						//chyba, ze sie myle
	
	void Board::copyField(int out[N][M]);
	
	unsigned int getWidth();
	unsigned int getHeight();


};



#endif 
