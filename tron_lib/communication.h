#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <cstdio>
#include <winsock2.h>
#include <cstdlib>
#include <Board.h>


#pragma comment(lib, "Ws2_32.lib")

DWORD WINAPI run(void *argumenty);

struct direction //dane wysylane od gracza do serwera
{
	int directionX;
	int directionY;
};

struct recevData //dane wysy�ane z serwera do gracza
{
	int field[N][M];
	bool end=false;
	bool start=false;
};

struct dataToThread //dane do w�tku
{
	SOCKET mySocket;
	recevData toSend;
	direction direction;
	CRITICAL_SECTION CriticalSection;
	bool next;
	bool send;
	bool start;
	int numberOfPleyer;
};


class communication{
	private:
		WSADATA wsaData;
		SOCKET mainSocket;
		sockaddr_in service;

	public:
		communication(char* ip, int port);
		communication(SOCKET mainSocket);

		void sendRecevData(recevData toSend);
		void sendDirection(direction toSend);
		direction receiveDirection();
		recevData receiveRecevData();
		void conect();		

};

class server{
	private:
		WSADATA wsaData;
		SOCKET mainSocket;
		dataToThread *data;
		sockaddr_in service;
		int numberOfPlayers;
		HANDLE *thread;
		CRITICAL_SECTION CriticalSection;

	public:
		server(char* ip, int port, int numberOfPleyers, CRITICAL_SECTION CriticalSection);
		void acceptPleyers();
		dataToThread* getData();
		
};